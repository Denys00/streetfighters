import Fighter from '../fighter';
import { fighterService } from './fightersService';

export default class FightService{
    constructor(){
        
    }

    startFight(firstFighter: Fighter, secondFighter: Fighter){
        var firstFighterHealth  = firstFighter.health;
        var secondFighterHealth = secondFighter.health;

        while(firstFighterHealth >= 0 || secondFighterHealth >=0){
            firstFighterHealth -= secondFighter.getHitPower() - firstFighter.getBlockPower();
            secondFighterHealth -= firstFighter.getBlockPower() - secondFighter.getBlockPower();
        }
        if(firstFighterHealth > 0){
            document.getElementById("winner").innerHTML = firstFighter.name;
        }
        else{
            document.getElementById("winner").innerHTML = secondFighter.name;
        }
    }
    
}