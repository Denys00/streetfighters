import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();


    //this.fightersDetailsMap = new Map();
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  setFighter(fighter) {
    if (!document.getElementById("fighter1Id").innerText) {
      document.getElementById("fighter1Id").innerText = fighter._id;
      document.getElementById("fighter1Name").innerText = fighter.name;
      document.getElementById("fighter1Health").innerText = fighter.health;
    }
    else{
      document.getElementById("fighter2Id").innerText = fighter._id;
      document.getElementById("fighter2Name").innerText = fighter.name;
      document.getElementById("fighter2Health").innerText = fighter.health;
    }
  }

  printData(fighter){
    alert(`
    Id: ${fighter._id}
    Name: ${fighter.name}
    Health: ${fighter.health}
    Attack: ${fighter.attack}
    Defense: ${fighter.defense}`);
  }

  handleFighterClick(event, fighter) {
    if (!this.fightersDetailsMap.has(fighter._id)) {

      fighterService.getFighterDetails(fighter._id)
        .then(mess =>
          this.fightersDetailsMap.set(fighter._id, mess));
    }
    this.setFighter(this.fightersDetailsMap.get(fighter._id));
    this.printData(this.fightersDetailsMap.get(fighter._id));

  }
}

export default FightersView;