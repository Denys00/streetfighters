export default class Fighter{
    id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    
    getHitPower(): number{
        return this.attack*(Math.floor(Math.random() * 2) + 1);
    }

    getBlockPower(): number{
        return this.defense*(Math.floor(Math.random() * 2) + 1);
    }
}