import FightersView from './fightersView';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
     this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      document.getElementById('startBtn').addEventListener("click", function() {console.log("Ok")});
      document.getElementById('clearBtn').addEventListener("click", function() {
        document.getElementById("fighter1Id").innerText = null;
          document.getElementById("fighter1Name").innerText = null;
          document.getElementById("fighter1Health").innerText = null;
        
          document.getElementById("fighter2Id").innerText = null;
          document.getElementById("fighter2Name").innerText = null;
          document.getElementById("fighter2Health").innerText = null;
      });

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
  }


export default App;